import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Actor } from './actor';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActoresService {
  private arrayActores: Actor[] = [];
  public avisos: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(private http: HttpClient) {
    this.http.get<Actor[]>('http://localhost:3100/actores').pipe(
      map(
        actores => {
          return actores.map(
            actor => {
              return {
                nombre: actor.nombre,
                img: actor.img,
                off: false
              };
            }
          );
        }
      )
    )
    .subscribe((actores) => {
      this.arrayActores = actores;
      this.avisos.next('actores-cargados');
    });
  }

  public get actores(): Actor[] {
    return this.arrayActores;
  }

  public toggleOff(actor: Actor): void {
    actor.off = !actor.off;
  }

}
