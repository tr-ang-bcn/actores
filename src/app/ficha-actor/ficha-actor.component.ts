import { Component, OnInit, Input } from '@angular/core';
import { Actor } from '../actor';
import { ActoresService } from '../actores.service';

@Component({
  selector: 'act-ficha-actor',
  templateUrl: './ficha-actor.component.html',
  styleUrls: ['./ficha-actor.component.css']
})
export class FichaActorComponent implements OnInit {

  @Input()
  public actor: Actor;



  constructor(private actoresService: ActoresService) { }

  ngOnInit() {
  }

public toggleOff(): void {
  this.actoresService.toggleOff(this.actor);
}

}
