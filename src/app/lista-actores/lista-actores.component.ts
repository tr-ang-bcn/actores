import { Component, OnInit } from '@angular/core';
import { Actor } from '../actor';
import { ActoresService } from '../actores.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'act-lista-actores',
  templateUrl: './lista-actores.component.html',
  styleUrls: ['./lista-actores.component.css']
})
export class ListaActoresComponent implements OnInit, OnDestroy {

  private arrayService: Actor[];
  private tempActores: Actor[];
  private index = 0;
  private suscripcionActores: Subscription;

  constructor(private actoresService: ActoresService) { }

  ngOnInit() {
    this.suscripcionActores = this.actoresService.avisos.subscribe(
      msj => {
        if (msj === 'actores-cargados') {
          this.arrayService = this.actoresService.actores;
          this.tempActores = [];

          this.arrayService.forEach((actor, index) => {
            setTimeout(() => {
              this.tempActores.push(actor);
            }, 1000 * (index + 1));
          });
        }
      }
    );
  }

  public get arrayActores(): Actor[] {
    return this.tempActores;
  }

  ngOnDestroy(): void {
    this.suscripcionActores.unsubscribe();
  }

}
